<?php

namespace wishlist\vue;
use wishlist\modele\Reservation;
use wishlist\modele\Message;
use wishlist\modele\Utilisateur;

class vueListe{
	private $liste;
	private $item;
	
	public function __construct($liste){
		$this->liste=$liste;
	}
	
	private function afficherListe(){
		$res = '<h3> Liste: '.$this->liste->titre .' Description : '.$this->liste->description .' Expiration : '.$this->liste->expirations.'</h3>';
		return $res;
	}
	
	private function afficherListes(){
		$res = '<section>';
		foreach($this->liste as $list){
			$res = $res . '<a href="liste/'.$list->token.'">'.$list->titre.'</a>  '.$list->description .' <br><br>';
		}
		$res = $res . '</section>';
		return $res;
	}
	
	private function afficherItems(){
		$app =\Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
        $url = $rootUri."/web/image";
		
		$res = "<section>";
           foreach($this->liste->items as $item ) {
               $res = $res . '<p> Id item : '.$item->id. ' Nom item : ' .$item->nom.'</p><br>';
			   $urlImage = $url . '/'.$item->img;
			   $res = $res . ' <img src="'.$urlImage.'"height="250" width="250">';
								
			   $reservation = Reservation::where('id_item','=',$item->id)->first();
		
			   if(empty($reservation)){
					   if(isset($_SESSION['account'])){
							$utilisateur = Utilisateur::where('pseudo','=',$_SESSION['account']->pseudo)->first();
					   }
					   if(isset($utilisateur)){
						   if($this->liste->user_id != $utilisateur->idUser){
								$resa = '<br><input type="submit" value="Reserver l\'item">';
						   }
						   else{
							   $resa = '';
							    $res = $res . ' <form method="get" action="'.$rootUri.'/item/set/'.$item->id.'/'.$this->liste->token.'">
									<input type="submit" value="Modifier l\'item">
								</form>';
						   }
					   }
					   else{
						   $resa = '<br><input type="submit" value="Reserver l\'item">';
					   }
			   }
			   else{
				   $resa = '<p> Cet article est réservé par '.$reservation->nom.'<p>';
			   }
			   
			   $res = $res . ' <form method="get" action="'.$rootUri.'/item/reservation/'.$item->id.'">
									'.$resa.'
								</form>';								
           }
        $res = "$res </section>";
        return $res;
	}
	
	public function setItem($item){
		$this->item=$item;
	}

	
	public function render($selecteur){
		$app =\Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
		switch($selecteur){
			case 1 : {
				$content = $this->afficherListe();
				$content = $content . ' <p> Les messages : </p> ';
				if(isset($_SESSION['account'])){
				$utilisateur = Utilisateur::where('idUser','=',$this->liste->user_id)->first();
					if($utilisateur->pseudo == $_SESSION['account']->pseudo){
						$content = $content . '<form method="get" action="'.$rootUri.'/liste/setListe/'.$this->liste->token.'">
											<input type="submit" value="Modifier la liste">
										</form>';
						$content = $content . '<form method="get" action="'.$rootUri.'/item/add/'.$this->liste->token.'">
											<br><input type="submit" value="Ajouter un item">
							</form>';
					}
				}
				$messages = Message::where('id_liste','=',$this->liste->no)->get();
				foreach($messages as $message){
					$membre = Utilisateur::where('idUser','=',$message->id_membre)->first();
					if(!empty($membre)){
					$pseudo = $membre->pseudo;
					$content = $content . '<p> '.$message->message .' <b> <i>par '.$pseudo.' </b> </i><br>';
					}
				}
				if(isset($_SESSION['account'])){
				$content = $content . '<form method="post" action="'.$rootUri.'/liste/sendMessage/'.$this->liste->no.'">
											<br><input type="text" name="lsMessage" required><br><br>
											<input type="submit" value="Envoyer">
							</form>';
				}
				
				$content = $content . $this->afficherItems();
				break;
			}
			case 2 : {
				$content =  $this->afficherListe();
				$content = $content.'<form method="post" action="'.$rootUri.'/liste/setListe/'.$this->liste->token.'">
								Titre : <input type="text" name="newTitre"> <br> <br>
								Description : <input type="test" name="newDescription"> <br> <br>
								Expiration : <input type="date" name="newExpiration"> <br><br>
								<input type="submit" value="Validez">
							</form>';
				break;
			}
			case 3 : {
				$content = $this->afficherListe();
				$content = $content.'<form method="post" action="'.$rootUri.'/liste/setListe/'.$this->liste->token.'">
								Titre : <input type="text" name="newTitre"> <br> <br>
								Description : <input type="test" name="newDescription"> <br> <br>
								Expiration : <input type="date" name="newExpiration"> <br><br>
								<input type="submit" value="Validez">
								<p>Votre liste a été modifié </p>
							</form>';
				break;
			}
			case 4 : {
				$content = '<h1> Ajouter un item </h1>';
				$content = $content . $this->afficherListe();
				$content = $content.'<form method="post" action="'.$rootUri.'/item/add/'.$this->liste->token.'">
								Titre : <input type="text" name="itemTitre"> <br> <br>
								Description : <input type="test" name="itemDescription" > <br> <br>
								Prix : <input type="number" name="itemPrix"> <br><br>
								<input type="submit" value="Validez">
							</form>';
				break;
			}
			case 5 : {
				$content = '<h1> Modification de l\'item </h1>';
				$content = $content.'<form method="post" action="'.$rootUri.'/item/set/'.$this->item->id.'/'.$this->liste->token.'">
								Titre : <input type="text" name="itemTitre"> <br> <br>
								Description : <input type="test" name="itemDescription"> <br> <br>
								Prix : <input type="number" name="itemPrix"> <br><br>
								<input type="submit" value="Validez">
							</form>';
				break;
			}
			case 6 : {
				$pseudo = '';
				if(isset($_SESSION['pseudo'])){
					$pseudo = $_SESSION['pseudo'];
				}
				$content = '<h1> Reserver un item</h1>';
				
				$content = $content.'<form method="post" action="'.$rootUri.'/item/reservation/'.$this->item->id.'">
								Pseudo : <input type="text" name="pseudoReservation" value="'.$pseudo.'"> <br> <br>
								Message : <input type="test" name="messageReservation"> <br> <br>
								<input type="submit" value="Reserver">
							</form>';
				break;
			}
			
			case 7 : {
				$content = $this->afficherListes();
				break;
			}
		}
		
		$html = <<<END
<!DOCTYPE html>
<html>
	<head> <title>Liste de souhait</title> </head>
	
<body>
	<div>
		$content
	</div>
</body></html>
END;
	echo $html;
	}
	
}