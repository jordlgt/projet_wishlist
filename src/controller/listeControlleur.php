<?php

namespace wishlist\controller;

use wishlist\modele\Liste;
use wishlist\vue\vueListe;
use wishlist\vue\vueItem;
use wishlist\vue\vueCreateur;
use wishlist\modele\utilisateur;
use wishlist\modele\Item;
use wishlist\modele\Reservation;
use wishlist\modele\Message;

class listeControlleur{
	// Affichage pour vueCreateur
	public function afficher($selecteur){
		$vueCreateur = new vueCreateur();
		$vueCreateur->render($selecteur);
	}
	// Affichage pour vueListe
	public function afficherItemSetForm($id,$token){
		$item = Item::where('id','=',$id)->first();
		$liste = Liste::where('token','=',$token)->first();
		$vueListe = new vueListe($liste);
		$vueListe->setItem($item);
		$vueListe->render(5);
	}

	public function afficherItemForm($token){
		$liste = Liste::select('*')->where('token','=',$token)->first();
		$vueListe = new vueListe($liste);
		$vueListe->render(4);
	}
	
	
	public function afficherItem($idItem,$token){
		$item = Item::where('id','=',$idItem)->first();
		$liste = Liste::where('token','=',$token)->first();
		if($liste->no == $item->liste_id){
			$vueItem = new vueItem($item);
			$vueItem->render(1);
		}
	}
	
	public function afficherSetListe($token){
		$listes = Liste::get();
		foreach($listes as $liste){
			if($liste->token==$token){
				$vueListe = new vueListe($liste);
				$vueListe->render(2);
			}
		}
	}
	public function afficherListe($token){
		$listes = Liste::get();
		foreach($listes as $liste){
			if($liste->token==$token){
				$vueListe = new vueListe($liste);
				$vueListe->render(1);
			}
		}
	}
	
	public function afficherFormulaireResa($id){
		$item = Item::where('id','=',$id)->first();
		$idListe = $item->liste_id;
		$liste = Liste::where('no','=',1);
		$vueListe = new vueListe($liste);
		$vueListe->setItem($item);
		$vueListe->render(6);
	}
	
	public function reserverItem($id,$nom,$message){
		if(!isset($_SESSION['pseudo'])){
			$_SESSION['pseudo'] = $nom;
		}
		$reservation = new Reservation();
		$reservation->nom = $nom;
		$reservation->message = $message;
		$reservation->id_item=$id;
		$reservation->save();
	}
	
	public function creerItem($titre,$description,$prix,$token){
		$liste = Liste::select('*')->where('token','=',$token)->first();
		$item = new Item();
		$item->liste_id=$liste->no;
		$item->nom = $titre;
		$item->descr = $description;
		$item->tarif = $prix;
		$item->save();
		$vueListe = new vueListe($liste);
		$vueListe->render(1);
	}
	
	public function creerListe($titre, $desc, $expire,$etat){ 
	$utilisateur = utilisateur::select('idUser')->where('pseudo','=',$_SESSION['account']->pseudo)->first();
	$liste=new Liste();	
	$liste->user_id = $utilisateur->idUser;
	$liste->titre=$titre;
	$liste->description=$desc;
	$liste->expiration=$expire;
	$liste->token=bin2hex(random_bytes(4));
	$liste->etat=$etat;
	$liste->save();
	$app =\Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
		header('Location: '.$rootUri.'/');
		exit;
	}

	public function modifierListe($newTitre,$newDescription,$newExpiration,$token){
		$listemodif = Liste::select('*')->where('token','=',$token)->first();
		if(!empty($newTitre)){
			$listemodif->titre=$newTitre;	
		}
		if(!empty($newTitre)){
			$listemodif->description=$newDescription;
		}
		if(!empty($newTitre)){
			$listemodif->expiration=$newExpiration;
		}
		$listemodif->save();
		$vueListe = new vueListe($listemodif);
		$vueListe->render(3);
 
	}
	
	public function setItem($itemTitre,$itemDescription,$itemPrix,$id,$token){
		$listemodif = Liste::where('token','=',$token)->first();
		$item = Item::select('*')->where('id','=',$id)->first();
		
		if(!empty($itemTitre)){
			$item->nom = $itemTitre;
		}
		if(!empty($itemDescription)){
			$item->descr = $itemDescription;
		}
		if(!empty($itemTitre)){
			$item->tarif = $itemPrix;
		}
		$item->save();
		$app =\Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
		header('Location: '.$rootUri.'/');
		exit;
	}
	
	public function sendMessage($idliste,$messageliste){
		$app =\Slim\Slim::getInstance();
        $rootUri = $app->request->getRootUri();
		$message = new Message();
		$utilisateur = Utilisateur::where('pseudo','=',$_SESSION['account']->pseudo)->first();
		$message->id_liste=$idliste;
		$message->id_membre = $utilisateur->idUser;
		$message->message=$messageliste;
		$message->save();
		$liste = Liste::select('token')->where('no','=',$idliste)->first();
		$token = $liste->token;
		header('Location: '.$rootUri.'/liste/'.$token.'');
		exit;
		
	}
	
	public function afficherListePublique(){
		$liste = Liste::where('etat','=','publique')->get();
		$vueListe = new vueListe($liste);
		$vueListe->render(7);
	}
	
}