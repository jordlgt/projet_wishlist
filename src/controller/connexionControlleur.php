<?php

namespace wishlist\controller;

use wishlist\vue\vueCompte;
use wishlist\modele\Utilisateur;

class connexionControlleur{
	
	private $vueCompte;
	
	public function __construct(){
		$this->vueCompte = new vueCompte();
	}
	
	public function affichage($selecteur){
		$this->vueCompte->render($selecteur);
	}
	
	
	public function seConnecter($pseudo,$password){

		$utilisateur = Utilisateur::where('pseudo','=',$pseudo)->first();
		if(!empty($utilisateur)){
			if($utilisateur->pseudo == $pseudo){
				if(password_verify($password,$utilisateur->password)){
					$_SESSION['account']=$utilisateur;
					$this->vueCompte->render(4);
				}
				else{
					$this->vueCompte->render(3);
				}
			}
			else{
				$this->vueCompte->render(3);
			}
		}
		else{
			$this->vueCompte->render(3);
		}
	}
	
	public function inscription($pseudo,$password){
		$utilisateur = new utilisateur();
		$utilisateur->pseudo = $pseudo;
		$utilisateur->password = $password;
		$utilisateur->save();
		$_SESSION['account'] = $utilisateur;
		$this->vueCompte->render(8);
	}
	
	public function seDeconnecter(){
		unset($_SESSION['account']);
		session_destroy();
		$this->vueCompte->render(6);
	}
	
	public function modifierPassword($oldPassword,$newPassword){
		$utilisateur = Utilisateur::select('idUser','password')->where('pseudo','=',$_SESSION['account']->pseudo)->first();
		if(password_verify($oldPassword,$_SESSION['account']->password)){
			$utilisateur->password = $newPassword;
			$utilisateur->save();
			$this->seDeconnecter();
			
		}
		else{
			echo 'Mot de passe incorrecte';
		}
	}

}
