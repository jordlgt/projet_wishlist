<?php

namespace wishlist\controller;

use wishlist\modele\Item;

class ControlItem {

  /**
   * fonction ajouterItem qui permet de creer un item et de l'ajouter dans la liste
   * @param $id_liste l'id de la liste a laquelle on ajoute l'item
   * @param $nom le nom de l'Item
   * @param $desc la description de l'Item
   * @param $rpix le prix de l'Item
   * @param $url l'url de l'Item
   * @return $item l'Item cree
   */
  public function ajouterItem($id_liste,$nom,$desc,$prix,$url){
    $item=new Item();

    $item->liste_id=$id_liste;
    $item->nom=$nom;
    $item->descr=$desc;
    $item->tarif=$prix;
    $item->url=$url;

    $item->save();

    return $item;
  }

  /**
   * fonction modifierItem permettant de modifier un item (mit en parametre)
   * @param $itemmodif l'item a modifier
   * @param $nouv la valeur de l'attribut a modifier
   * @param $zonemodifiee l'attribut a modifier
   *          si =1 -> nom
   *          si =2 -> description
   *          si =3 -> tarif
   */
  public function modifierItem($itemmodif,$nouv,$zonemodifiee){
    switch($zonemodifiee) {
			case 1:
				$itemmodif->nom=$nouv;
				break;

			case 2:
				$itemmodif->descr=$nouv;
				break;

      case 3:
  			$itemmodif->tarif=$nouv;
  			break;
		}
    $itemmodif->save();
	}


	/**
     * fonction supprimerItem qui permet de supprimer un item de la table
     * @param $itemsup l'item que l'on va supprimer
     */
  public function supprimerItem($itemsup){
    $itemsup->delete();
  }


  /**
   * fonction modifierImage qui permet de modifier ou ajouter une image liee a un item
   * @param $itemmodif l'item qui possede l'image que l'on va modifier
   * @param $newUrl le nouvel URL vers la nouvelle image
   */
  public function modifierImage($itemmodif, $newUrl){
        $itemmodif->img=$newUrl;
        $itemmodif->save();
  }

  /**
   * fonction supprimerImage qui permet de supprimer une image liee a un item
   * @param $itemmodif l'item qui possede l'image que l'on va supprimer
   */
  public function supprimerImage($itemmodif){
      $itemmodif->img="";
      $itemmodif->save();
  }

}
