<?php

namespace wishlist\controller;

use wishlist\modele\Liste;

class ControlListe {
	
	//note a rappeler :
	// $tokmod = token pour verifier si c'est un moderateur de la liste
	//$tokvis = token pour verifier si c'est un visiteur
	
	/**
	 * fonction creerListe permettant de creer une liste
     * @param $titre le titre de la liste
     * @param $desc la description de la liste
     * @param $expire la date d'expiration de la liste
     * @param $token l'acces permettant de modifier la liste
	 */
	public function creerListe($titre, $desc, $expire, $token){ 
	$liste=new Liste();	
	
	$liste->titre=$titre;
	$liste->description=$desc;
	$liste->expiration=$expire;
	$liste->token=$token;
	
	$liste->save();
	
	return $liste; 	
	}

    /**
     * fonction modifierListe permettant de modifier une liste (mise en parametre)
     * @param $listemodif la liste a modifier
     * @param $nouv la valeur de l'attribut a modifier
     * @param $zonemodifiee l'attribut a modifier
     *          si =1 -> titre
     *          si =2 -> description
     *          si =3 -> expiration
     */
	public function modifierListe($listemodif, $nouv, $zonemodifiee){
		switch($zonemodifiee) {
			case 1:
				$listemodif->titre=$nouv;
				break;
			
			case 2:
				$listemodif->description=$nouv;
				break;
				
			case 3:
				$listemodif->expiration=$nouv;
				break;
		}
        $listemodif->save();
	}
}