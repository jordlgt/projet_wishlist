<?php

namespace wishlist\vue;

class VueFormulaireListe {

/**
 * fonction render qui permet d'afficher la fenetre du formulaire
 * de creation de liste
 */
    public function render() {
        $html = <<<END
<!DOCTYPE html>
<html>
	<head> <title>Creer une Liste</title> <link rel="stylesheet" href="Formulaire.css" /> </head>
<body>

  <h1>Creer une Liste</h1>

	<form action="/formulaireListe" method="post">
        <div>
            <label for="titre">Titre :</label>
            <input type="titre" id="title" name="title">
        </div>

        <div>
            <label for="desc">Description :</label>
            <textarea id="desc" name="desc"></textarea>
        </div>

        <div>
            <label for="date_exp">Date d'expiration :</label>
            <input type="date_exp" id="date_exp" name="date_exp">
        </div>

        <div class="bouton">
            <button type="submit">Creer la Liste</button>
        </div>
</form>
</body></html>
END;
        echo $html;
    }
}
