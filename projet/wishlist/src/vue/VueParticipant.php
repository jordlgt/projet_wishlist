<?php

namespace wishlist\vue ;

class VueParticipant {
	
	private $objets;

    /**
     * VueParticipant constructor.
     * @param $tab
     */
    public function __construct($tab) {
		$this->objets = $tab;
	}

    /**
     * fonction affichageListe2souhaits
     *      (affiche la liste des liste de souhaits)
     * @return string
     */
    private function affichageListe2souhaits() {
		$res='';
		foreach($this->objets as $li) {
		    $res = $res."<section>";
		    foreach($li as $val) {
                $res = $res . $val . '<br>';
            }
            $res = $res."</section>";
		}
		return $res;
	}

    /**
     * fonction affichageListeItems
     *      (affiche la liste des items)
     * @return string
     */
    private function affichageListeItems() {
		$res='';
		foreach($this->objets as $i) {
			foreach ($i as $val) {
				$res = $res.$val."<br>";
			}
		}
		$res = "<section>$res</section>";
		return $res;
	}

    /**
     * fonction affichage1Item
     *      (affiche 1 item)
     * @return string
     */
    private function affichage1Item() {
	    $res='<section>';
	    foreach($this->objets as $val){
	        $res = $res.$val."<br>";
        }
        $res = $res."</section>";
	    return $res;
    }

    /**
     * fonction render
     *      (appel une des 3 methodes au dessus
     *       en fonction du selecteur entre en parametre)
     * @param $selecteur
     */
    public function render($selecteur) {
		switch ($selecteur) {
			case 1 : {
				$content = $this->affichageListe2souhaits();
				break;
			}
			case 2 : {
				$content = $this->affichageListeItems();
				break;
			}
            case 3 : {
                $content = $this->affichage1Item();
                break;
            }
		}
		$html = <<<END
<!DOCTYPE html>
<html>
	<head> <title>Content</title> </head>
<body>
	<div class="content">
		$content
	</div>
</body></html>
END;
		echo $html;
	}
	
}