<?php

namespace wishlist\vue;

class VueFormulaireItem {

/**
 * fonction render qui permet d'afficher la fenetre du formulaire
 * de l'ajout d'une liste
 */
    public function render() {
        $html = <<<END
<!DOCTYPE html>
<html>
	<head> <title>Ajouter un Item</title> <link rel="stylesheet" href="Formulaire.css" /> </head>
<body>

  <h1>Ajouter un Item</h1>

	<form action="/formulaireItem" method="post">
        <div>
            <label for="titre">Nom :</label>
            <input type="nom" id="name" name="name">
        </div>

        <div>
            <label for="desc">Description :</label>
            <textarea id="desc" name="desc"></textarea>
        </div>

        <div>
            <label for="prix">Prix :</label>
            <input type="prix" id="prix" name="prix">
        </div>

        <div class="bouton">
            <button type="submit">Ajouter l'Item</button>
        </div>
</form>
</body></html>
END;
        echo $html;
    }
}
