<?php
require 'vendor/autoload.php';

use \wishlist\modele\Item;
use \wishlist\modele\Liste;
use \Illuminate\Database\Capsule\Manager as DB;

$config=parse_ini_file('src/conf/conf.ini');
$db = new DB();
$db->addConnection( [
	'driver'	=> $config['driver'],
	'host'		=> $config['host'],
	'database'	=> $config['database'],
	'username'	=> $config['username'],
	'password'	=> $config['password'],
	'charset'	=> $config['charset'],
	'collation'	=> $config['collation'],
	'prefix'	=> ''
] );

$db->setAsGlobal();
$db->bootEloquent();

// $i represente le select * from item
$i=Item::get();

// $l represente SELECT * from liste
$l=Liste::get();

var_dump($i);
var_dump($l);

// affichage de l'item dont l'id est passe en parametre 
$iid = Item::where('id','=',$_GET['id'])->first();
var_dump($iid);


$items = Item::get();

foreach($items as $it){
	$item = Item::where('id','=',$it['id'])->first();
	$li = $item->liste;
	echo 'ID : '.$it['id'].'<br> Nom : '.$it['nom'].'<br> Description : '.$it['descr'].'<br> Liste : '.$li['titre'].'<br><br>';
}





