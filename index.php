<?php
/**
 * File:  index.php
 * Creation Date: 04/12/2017
 * description:
 *
 * @author: canals
 */

require_once __DIR__. '/vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use wishlist\controller\listeControlleur;
use wishlist\controller\connexionControlleur;
use wishlist\modele\utilisateur;

session_start();

$tab = parse_ini_file('src/conf/conf.ini');

$db = new DB();
$db->addConnection( $tab );
$db->setAsGlobal();
$db->bootEloquent();

$app = new Slim\Slim();

$app->get('/',function(){
	$connexionControlleur = new connexionControlleur();
	if(isset($_SESSION['account'])){
		$connexionControlleur->affichage(5);
	}
	else{
		$connexionControlleur->affichage(9);
	}
});

$app->get('/login',function(){
	$connexionControlleur = new connexionControlleur();
	if(!isset($_SESSION['account'])){
		$connexionControlleur->affichage(1);
	}
	else{
		$connexionControlleur->affichage(5);
	}
});

$app->get('/inscription',function(){
	$connexionControlleur = new connexionControlleur();
	$connexionControlleur->affichage(2);
});

$app->post('/inscription',function(){
	$connexionControlleur = new connexionControlleur();
	$connexionControlleur->inscription(filter_var($_POST['login'],FILTER_SANITIZE_STRING),password_hash($_POST['password'], PASSWORD_DEFAULT));
})->name('inscription');

$app->get('/accueil',function(){
	$connexionControlleur = new connexionControlleur();
	if(isset($_SESSION['account'])){
		$connexionControlleur->affichage(5);
	}
});

$app->post('/accueil',function(){
	$connexionControlleur = new connexionControlleur();
	$connexionControlleur = $connexionControlleur->seConnecter(filter_var($_POST['login'],FILTER_SANITIZE_STRING),filter_var($_POST['password'],FILTER_SANITIZE_STRING));
})->name('accueil');

$app->get('/logout',function(){
	$connexionControlleur = new connexionControlleur();
	$connexionControlleur->seDeconnecter();

});

$app->get('/setAccount',function(){
	$connexionControlleur = new connexionControlleur();
	if(isset($_SESSION['account'])){
	$connexionControlleur->affichage(7);
	}
});

$app->post('/setAccount',function(){
	$connexionControlleur = new connexionControlleur();
	$connexionControlleur->modifierPassword($_POST['oldPassword'],password_hash($_POST['newPassword'], PASSWORD_DEFAULT));
})->name('setAccount');

$app->get('/liste',function(){
	$listeControlleur = new listeControlleur();
	$listeControlleur->afficherListePublique();
});

$app->get('/liste/nouvelleListe',function(){
	$listeControlleur = new listeControlleur();
	$listeControlleur->afficher(1);
	
});

$app->post('/liste/nouvelleListe',function(){
	$listeControlleur = new listeControlleur();
	$listeControlleur->creerListe($_POST['titre'],$_POST['description'],$_POST['expiration'],$_POST['etat']);
})->name('nouvelleListe');

$app->get('/liste/setListe/:token',function($token){
	$listeControlleur = new listeControlleur();
	$listeControlleur->afficherSetListe($token);
});


$app->post('/liste/setListe/:token',function($token){
	$listeControlleur = new listeControlleur();
	$listeControlleur->modifierListe($_POST['newTitre'],$_POST['newDescription'],$_POST['newExpiration'],$token);
});


$app->get('/liste/:token',function($token){
	$listeControlleur = new listeControlleur();
	$listeControlleur->afficherListe($token);
});

// Affichage du formulaire pour ajouter un item
$app->get('/item/add/:token',function($token){
	$listeControlleur = new listeControlleur();
	$listeControlleur->afficherItemForm($token);
});

$app->post('/item/add/:token',function($token){
	$listeControlleur = new listeControlleur();
	$listeControlleur->creerItem($_POST['itemTitre'],$_POST['itemDescription'],$_POST['itemPrix'],$token);
});

// Modifier un item

$app->get('/item/set/:id/:token',function($id,$token){
	$listeControlleur = new listeControlleur();
	$listeControlleur->afficherItemSetForm($id,$token);
});

$app->post('/item/set/:id/:token',function($id,$token){
	$listeControlleur = new listeControlleur();
	$listeControlleur->setItem($_POST['itemTitre'],$_POST['itemDescription'],$_POST['itemPrix'],$id,$token);
});

// Reserver un item

$app->get('/item/reservation/:id',function($id){
	$listeControlleur = new listeControlleur();
	$listeControlleur->afficherFormulaireResa($id);
});

$app->post('/item/reservation/:id',function($id){
	$listeControlleur = new listeControlleur(); 
	$listeControlleur->reserverItem($id,$_POST['pseudoReservation'],$_POST['messageReservation']);
});

// Envoyer un messageReservation
$app->post('/liste/sendMessage/:listeid',function($listeid){
	$listeControlleur = new listeControlleur();
	$listeControlleur->sendMessage($listeid,$_POST['lsMessage']);
});

// Affichage d'un item
$app->get('/item/aff/:token/:id',function($token,$id){
	$listeControlleur = new listeControlleur();
	$listeControlleur->afficherItem($id,$token);
});



$app->run();
